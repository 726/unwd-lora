/**
  ******************************************************************************
  * @file           : 
  * @brief          : serial print (uart->usb) printing for debugging
  ******************************************************************************
  * @author akeela
  *
  ******************************************************************************
  */

#ifndef __serial_H
#define __serial_H

#ifdef __cplusplus
  extern "C" {
#endif

#include <stdint.h>
#include <string.h>
#include <inttypes.h>
#include <stdio.h>
#include "stdarg.h"

#include "main.h"
#include "uart.h"


void serialInit(void);
void serialPrint(char *msg, ...);
void serialPrint2(char *msg, ...);
uint8_t serialReadByte(void);

#endif /*__ serial_H */
