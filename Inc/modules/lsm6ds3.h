/**
  ******************************************************************************
  * @file           : 
  * @brief          : LSM6DS3 acc module
  ******************************************************************************
  * @author akeela
  *
  ******************************************************************************
  */

#ifndef __LSM6DS3_H
#define __LSM6DS3_H

#ifdef __cplusplus
  extern "C" {
#endif

#include <stdint.h>

#include "main.h"
#include "lsm6ds3_reg.h"

//i2c addr
#define LSM6DS3_ADDRESS 0x6A

struct lsm6ds3_params
{
  uint8_t gyro_enabled;
  uint16_t gyro_range;
  uint16_t gyro_sample_rate;
  uint16_t gyro_bandwidth;

  uint8_t accel_enabled;
  uint16_t accel_range;
  uint16_t accel_sample_rate;
  uint16_t accel_bandwidth;
};


struct lsm6ds3_data
{
  int acc_x;
  int acc_y;
  int acc_z;

  int gyr_x;
  int gyr_y;
  int gyr_z;
};


uint8_t lsm6ds3_Init(void);
void lsm6ds3_Configure(void);
void lsm6ds3_Read(void);
uint16_t lsm6ds3_ReadRawAccelX(void);
uint16_t lsm6ds3_ReadRawAccelY(void);
uint16_t lsm6ds3_ReadRawAccelZ(void);

void lsm6ds3_FreefallDetectionEnable(void);
void lsm6ds3_OrientDetectionEnable(void);
void lsm6ds3_SignificantMotionDetectionEnable(void);
void lsm6ds3_MovementDetectionEnable(void);
uint8_t lsm6ds3_FreefallDetectionRead(void);
uint8_t lsm6ds3_OrientDetectionRead(void);
uint8_t lsm6ds3_SignificantMotionDetectionRead(void);
int lsm6ds3_CalcAccel(uint16_t input);

#endif /*__LSM6DS3_H */