/**
  ******************************************************************************
  * @file           : 
  * @brief          : opt3001 module
  ******************************************************************************
  * @author akeela
  *
  ******************************************************************************
  */

#ifndef __OPT3001_H
#define __OPT3001_H

#ifdef __cplusplus
  extern "C" {
#endif

#include <stdint.h>

#include "main.h"

/**
 * @brief Initial OPT3001 address on I2C bus
 */
#define OPT3001_ADDRESS 0x44 // ADDR = GND
/*
#define OPT3001_ADDRESS 0x45 // ADDR = VDD
#define OPT3001_ADDRESS 0x46 // ADDR = SDA
#define OPT3001_ADDRESS 0x47 // ADDR = SCL
*/

/* bytes are swapped */
#define OPT3001_CFG_FC_1    0x0000
#define OPT3001_CFG_FC_2    0x0100
#define OPT3001_CFG_FC_4    0x0200
#define OPT3001_CFG_FC_8    0x0300
#define OPT3001_CFG_MASK    0x0400
#define OPT3001_CFG_POLPOS  0x0800
#define OPT3001_CFG_LATCH   0x1000
#define OPT3001_CFG_FLAGL   0x2000
#define OPT3001_CFG_FLAGH   0x4000
#define OPT3001_CFG_CRF     0x8000
#define OPT3001_CFG_OVF     0x0001
#define OPT3001_CFG_SHDN    0x0000
#define OPT3001_CFG_SHOT    0x0002
#define OPT3001_CFG_CONT    0x0004
#define OPT3001_CFG_100MS   0x0000
#define OPT3001_CFG_800MS   0x0008
#define OPT3001_CFG_RNAUTO  0x00C0

#define OPT3001_CFG (OPT3001_CFG_FC_1 | OPT3001_CFG_SHOT | OPT3001_CFG_100MS | OPT3001_CFG_RNAUTO )
#define OPT3001_CFG_DEFAULT 0x10C8 //0xCE10

/**
 * @brief OPT3001 registers
 */
#define OPT3001_REG_RESULT        0x00
#define OPT3001_REG_CONFIG        0x01
#define OPT3001_REG_ID            0x7E
#define OPT3001_CHIP_ID           0x4954
#define OPT3001_REG_CONFIG_MASK   0xFE1F

uint8_t opt3001_Init(void);
void opt3001_Read(uint32_t *lux);
int opt3001_Calculate(uint16_t data);

#endif /*__OPT3001_H */