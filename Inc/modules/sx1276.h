/**
  ******************************************************************************
  * @file           : 
  * @brief          : SX1276 Driver
  ******************************************************************************
  * @author akeela, agentgosdepa
  *
  ******************************************************************************
  */

#ifndef __SX1276_H
#define __SX1276_H

#ifdef __cplusplus
  extern "C" {
#endif

#include <stdint.h>
#include "gpio.h"
#include "spi.h"
#include "timer.h"
#include "serial.h"
#include "eeprom.h"
#include "sx127x_registers.h"

void packet_CleanStruct(void);
void packet_SetAddr(void);
void packet_SetBME280(uint32_t temp, uint32_t pres, uint32_t humi);
void packet_SetOPT3001(uint32_t lux);
void packet_SetLSM6DS3(uint8_t state);
void packet_Debug1(void);
void packet_Debug2(void);
void packet_Debug3(void);
void SX1276_Delay(void);

void SX1276_SetFrequency(uint32_t freq);
void SX1276_SetHFMode(void);
void SX1278_CheckChannel(uint32_t freq, uint16_t rssi_thresh);

void SX1276_SetTx(void);
void SX1276_SetRx(void);

void SX1276_Init(void);
void SX1276_Reset(void);
void SX1276_Sleep(void);
void SX1276_Standby(void);
void SX1276_SetModeLora(void);
uint8_t SX1276_GetRSSI(void);

void SX1276_ReadFIFO(uint8_t* data, uint32_t len);
void SX1276_WriteFIFO(uint8_t* data, uint32_t len);

void SX1276_Transmit(void);//uint8_t* data, uint32_t len);
void SX1276_Recieve(uint8_t* buff, uint32_t* len);

#endif /*__SX1276_H */