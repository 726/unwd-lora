/**
  ******************************************************************************
  * @file           : 
  * @brief          : bme280 module
  ******************************************************************************
  * @author akeela
  *
  ******************************************************************************
  */

#ifndef __BME280_H
#define __BME280_H

#ifdef __cplusplus
  extern "C" {
#endif

#include <stdint.h>

#include "main.h"

// Адреса переферии будут перенесены в соответвующие файлы 

#define BME280_ADDRESS         0xEC
#define BME280_ADDRESS2 	   0x76

#define BME280_CTRL_HUM_ADDR   0xF2 // Humidity measure control register
#define BME280_CTRL_MEAS_ADDR  0xF4 // Control register pressure and temp measure
#define BME280_CONFIG_ADDR     0xF5
#define BME280_PRESS_ADDR      0xF7 //247
#define BME280_TEMP_ADDR       0xFA //250
#define BME280_HUM_ADDR        0xFD
#define BME280_TEMP_DIG_ADDR   0x88 //LSB_REG = 0x88 MSB_REG = 0x89
#define BME280_PRESS_DIG_ADDR  0x8E
#define BME280_HUM_DIG_ADDR1   0xA1 //161
#define BME280_HUM_DIG_ADDR2   0xE1 //225
#define BME280_ID_ADDR         0xD0
#define BME280_ID              0x60


typedef struct {
  uint16_t dig_T1;
  int16_t dig_T2;
  int16_t dig_T3;
  uint16_t dig_P1;
  int16_t dig_P2;
  int16_t dig_P3;
  int16_t dig_P4;
  int16_t dig_P5;
  int16_t dig_P6;
  int16_t dig_P7;
  int16_t dig_P8;
  int16_t dig_P9;
  uint8_t dig_H1;
  int16_t dig_H2;
  uint8_t dig_H3;
  int16_t dig_H4;
  int16_t dig_H5;
  int8_t dig_H6;
} bme_calib_data;

uint8_t bme280_Init(void);
void bme280_Calib(void);
void bme280_Read(int32_t *temp, uint32_t *pres, uint32_t *humi);
int32_t bme280_ReadTemp(int32_t *t_fine);
uint32_t bme280_ReadPres(int32_t *t_fine);
uint32_t bme280_ReadHum(int32_t *t_fine);

bme_calib_data bme_calib;

#endif /*__BME280_H */