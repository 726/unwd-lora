/**
  ******************************************************************************
  * @file           : 
  * @brief          : periph functions (i2c2)
  ******************************************************************************
  * @author akeela
  *
  ******************************************************************************
  */

#ifndef __PERIPH_H
#define __PERIPH_H

#ifdef __cplusplus
  extern "C" {
#endif

#include <stdint.h>
#include "i2c.h"


#define I2C_REQUEST_WRITE      0x00
#define I2C_REQUEST_READ       0x01

void periphInit(void);
//void i2c2Scan(void); // not implemented
void periphRead   (uint8_t slave_addr, uint8_t slave_reg, uint8_t *buf, uint16_t bytes_count);
uint8_t periphReadByte (uint8_t slave_addr, uint8_t slave_reg);
void periphWrite  (uint8_t slave_addr, uint8_t slave_reg, uint16_t bytes_write);
void periphWrite8 (uint8_t slave_addr, uint8_t slave_reg, uint8_t bytes_write);
//void i2c2Write(uint8_t slave_addr, uint8_t slave_reg, uint8_t *buf, uint16_t bytes_count);

#endif /*__PERIPH_H */