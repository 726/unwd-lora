/**
  ******************************************************************************
  * @file           : 
  * @brief          : EEPROM Driver
  ******************************************************************************
  * @author akeela
  *
  ******************************************************************************
  */

#ifndef __EEPROM_H
#define __EEPROM_H

#ifdef __cplusplus
  extern "C" {
#endif

#include <stdint.h>
#include "stm32l151xc.h"

#define FLASH_PEKEY1               (0x89ABCDEFU) //Flash program erase key1
#define FLASH_PEKEY2               (0x02030405U)

uint8_t EEPROM_Unlock(void);
uint8_t EEPROM_Lock(void);
uint8_t EEPROM_Erase(uint32_t TypeErase, uint32_t Address);
uint64_t EEPROM_Read(uint64_t address);
void EEPROM_Write(uint32_t address, uint32_t value);

#endif /*__EEPROM_H */