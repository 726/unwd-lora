/**
  ******************************************************************************
  * @file           : 
  * @brief          : SX1276 Driver
  ******************************************************************************
  * @author akeela, agentgosdepa
  *
  ******************************************************************************
  */

#ifndef __UART_H
#define __UART_H

#ifdef __cplusplus
  extern "C" {
#endif

#include <stdint.h>
#include "main.h"

void UART1_Init(void);
uint8_t UART1_Read(void);
void UART1_Write(uint8_t byte);

#endif /*__UART_H */