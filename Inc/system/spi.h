/**
  ******************************************************************************
  * @file           : 
  * @brief          : SX1276 Driver
  ******************************************************************************
  * @author akeela, agentgosdepa
  *
  ******************************************************************************
  */

#ifndef __SPI_H
#define __SPI_H

#ifdef __cplusplus
  extern "C" {
#endif

#include <stdint.h>
#include "main.h"

void SPI2_Init(void);
void SPI2_ReadBurst(uint8_t reg, uint8_t *outbuf, uint8_t len);
void SPI2_WriteBurst(uint8_t reg, uint8_t *inbuf, uint8_t len);
uint8_t SPI2_ReadByte(uint8_t reg);
void SPI2_WriteByte(uint8_t reg, uint8_t inreg);

#endif /*__SPI_H */