/**
  ******************************************************************************
  * @file           : 
  * @brief          : SX1276 Driver
  ******************************************************************************
  * @author akeela, agentgosdepa
  *
  ******************************************************************************
  */

#ifndef __GPIO_H
#define __GPIO_H

#ifdef __cplusplus
  extern "C" {
#endif

#include <stdint.h>
#include "stm32l151xc.h"

#define SX127X_DIO0 GPIO_PIN(PORT_A, 12)
#define SX127X_DIO1 GPIO_PIN(PORT_A, 13)
#define SX127X_DIO2 GPIO_PIN(PORT_A, 0)
#define SX127X_DIO3 GPIO_PIN(PORT_A, 6)
#define SX127X_DIO4 GPIO_PIN(PORT_A, 7)
#define SX127X_RESET GPIO_PIN(PORT_A, 8) 
#define SX127X_RFSWITCH GPIO_PIN(PORT_A, 11)

void SX1276_RFSwitchInit(void);
void SX1276_RFSwitchOff(void);
void SX1276_RFSwitchOn(void);

void SX1276_ResetOn(void);
void SX1276_ResetOff(void);

void SX1276_DI0Init(void);
void SX1276_DI1Init(void);
void SX1276_DI2Init(void);
void SX1276_DI3Init(void);
void SX1276_DI4Init(void);

void SX1276_DI0IRQ(void);
void SX1276_DI1IRQ(void);
void SX1276_DI2IRQ(void);
void SX1276_DI3IRQ(void);
void SX1276_DI4IRQ(void);

void Led_Init(void);
void Led_On(void);
void Led_Off(void);
void Led_ChangeState(void);

#endif /*__GPIO_H */