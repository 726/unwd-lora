/**
  ******************************************************************************
  * @file           : 
  * @brief          : SX1276 Driver
  ******************************************************************************
  * @author akeela, agentgosdepa
  *
  ******************************************************************************
  */

#ifndef __I2C_H
#define __I2C_H

#ifdef __cplusplus
  extern "C" {
#endif

#include <stdint.h>

#include "main.h"

#define I2C_REQUEST_WRITE      0x00
#define I2C_REQUEST_READ       0x01

void I2C2_Init(void);
void I2C2_Read(uint8_t slave_addr, uint8_t slave_reg, uint8_t *buf, uint16_t bytes_count);
void I2C2_Write(uint8_t slave_addr, uint8_t slave_reg, uint16_t bytes_write);
void I2C2_Write8(uint8_t slave_addr, uint8_t slave_reg, uint8_t bytes_write);

#endif /*__I2C_H */