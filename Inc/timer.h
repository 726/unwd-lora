/**
  ******************************************************************************
  * @file           : 
  * @brief          : periph functions (i2c2)
  ******************************************************************************
  * @author akeela
  *
  ******************************************************************************
  */

#ifndef __TIMER_H
#define __TIMER_H

#ifdef __cplusplus
  extern "C" {
#endif

#include <stdint.h>

void timerInit(void);
void Delay(uint32_t time);

#endif /*__TIMER_H */