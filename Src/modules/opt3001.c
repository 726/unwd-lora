#include "opt3001.h"

/**
  * @brief  Инициализация модуля opt3001
  */
uint8_t opt3001_Init(void)
{
  uint8_t buff[2];
  periphRead(OPT3001_ADDRESS, OPT3001_REG_ID, buff, 2);
  if (buff[1] != 0x49)
    return 0;
  periphWrite16(OPT3001_ADDRESS, OPT3001_REG_CONFIG, 0xCE10);

  //Delay for conf
  for (int i = 0; i < 500000; i++);
  
  return 1;
}

/**
  * @brief  opt3001
  */
void opt3001_Read(uint32_t *lux)
{
  uint8_t rd_value[20];
  uint16_t iData;
  //uint16_t iExponent, iMantissa;
  int lux_result;
  periphRead(OPT3001_ADDRESS, OPT3001_REG_RESULT, rd_value, 2);
  //[umdk-opt3001] Luminocity 41 lux
  iData = (rd_value[0] << 8) | rd_value[1];
  lux_result = opt3001_Calculate(iData);
  *lux = lux_result;
}

int opt3001_Calculate(uint16_t data)
{

    //calculate lux per LSB
    uint8_t exp = (data >> 12) & 0x0F;
    uint32_t lsb_size_x100 = (1 << exp);
    
    // remove 4-bit exponent and leave 12-bit mantissa only
    data &= 0x0FFF;
    
    return (((int)data * lsb_size_x100) / 100);
}