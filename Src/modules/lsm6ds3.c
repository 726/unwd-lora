#include "lsm6ds3.h"

/**
  * @brief  Инициализация модуля lsm6ds3
  */
uint8_t lsm6ds3_Init(void)
{
  uint8_t id;
  // 0x6A (or 0x6B)
  id = periphReadByte(LSM6DS3_ADDRESS, LSM6DS3_ACC_GYRO_WHO_AM_I_REG);

  for (int i = 0; i < 100000; i++);

  if (id != 0x69){
    return 0;  
  }
  lsm6ds3_Configure();
  return 1;
}

/**
  * @brief Настройка модуля
  * Включение акселерометра и гироскопа
  */
void lsm6ds3_Configure(void)
{
  uint8_t data = 0;
  //Включаю акселерометр

  data |= LSM6DS3_ACC_GYRO_BW_XL_200Hz; //dev->params.accel_bandwidth;
  data |= LSM6DS3_ACC_GYRO_FS_XL_2g; //dev->params.accel_range;
  data |= LSM6DS3_ACC_GYRO_ODR_XL_416Hz; //dev->params.accel_sample_rate;
  periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_CTRL1_XL, data);


  //Считываю ODR bit
  data = periphReadByte(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_CTRL4_C);

  data &= ~((uint8_t)LSM6DS3_ACC_GYRO_BW_SCAL_ODR_ENABLED);


  // Turn ODR ON?
  //if (accel_odr_off == 1) {
  //  data |= LSM6DS3_ACC_GYRO_BW_SCAL_ODR_ENABLED;
  //}

  periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_CTRL4_C, data);

  data = 0;
  data |= LSM6DS3_ACC_GYRO_IF_INC_ENABLED;

  periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_CTRL3_C, data);
  // Включаем гироскоп
  data = 0;

  data |= LSM6DS3_ACC_GYRO_BW_XL_100Hz;//dev->params.gyro_range;
  data |= LSM6DS3_ACC_GYRO_ODR_XL_26Hz;//dev->params.gyro_sample_rate;

  periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_CTRL2_G, data);
}

uint16_t lsm6ds3_ReadRawAccelX(void) 
{
  uint16_t output = 0;
  uint8_t buff[2];
  periphRead(LSM6DS3_ADDRESS, LSM6DS3_ACC_GYRO_OUTX_L_XL, buff,2 );
  output = (int16_t)buff[0] | (int16_t)(buff[1] << 8);
  return output;
}

uint16_t lsm6ds3_ReadRawAccelY(void) 
{
  uint16_t output = 0;
  uint8_t buff[2];
  periphRead(LSM6DS3_ADDRESS, LSM6DS3_ACC_GYRO_OUTY_L_XL, buff,2 );
  output = (int16_t)buff[0] | (int16_t)(buff[1] << 8);
  return output;
}

uint16_t lsm6ds3_ReadRawAccelZ(void) 
{
  uint16_t output = 0;
  uint8_t buff[2];
  periphRead(LSM6DS3_ADDRESS, LSM6DS3_ACC_GYRO_OUTZ_L_XL, buff,2 );
  output = (int16_t)buff[0] | (int16_t)(buff[1] << 8);
  return output;
}

int lsm6ds3_CalcAccel(uint16_t input) 
{
  int output = (int)(input * 0.000488);
  return output;
}

/**
  * @brief Включение датчика свободного падения
  */
void lsm6ds3_FreefallDetectionEnable(void)
{
  //uint8_t data = 0x33;
  //periphWrite8(LSM6DS3_ADDRESS, LSM6DS3_ACC_GYRO_FREE_FALL,data);
  //data = periphReadByte(LSM6DS3_ADDRESS, LSM6DS3_ACC_GYRO_FREE_FALL);
  //return data;
  //Включаем прерывания(сон)
  // Write 00h into WAKE_UP_DUR 
	periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_WAKE_UP_DUR, 0x00);

  periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_FREE_FALL, 0x33);
  
  periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_MD1_CFG, 0x10);
  periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_MD2_CFG, 0x10);

  //latch interrupt
  periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_TAP_CFG1, 0x01);
}

/**
  * @brief Включение датчика 6D (ориентации)
  */
void lsm6ds3_OrientDetectionEnable(void)
{
  // выключение акселерометра (уже включен в Configure)
  //periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_CTRL1_XL, 0x60);
  // устанавливаю 6D порог
  periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_TAP_THS_6D, 0x40);
  // включаю LPF2 фильтр ?!?!?!?
  //periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_TAP_CFG1, 0x10);
  //Нужно чтобы Freefall был также включенным поэтому 0x11
  periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_TAP_CFG1, 0x11);
  
  periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_CTRL8_XL, 0x01);
  // 6d прерывания 
  periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_MD1_CFG, 0x04);
}

/**
  * @brief Включение датчика движения
  */
void lsm6ds3_MovementDetectionEnable(void)
{
  //?
}

/**
  * @brief Включение датчика
  */
void lsm6ds3_SignificantMotionDetectionEnable(void)
{
  //выставить аксель на 26 Hz или выше
  // выключаю доступ к внутренним функциям
  // Write 80h into FUNC_CFG_ADDRESS (0x01)
  periphWrite8(LSM6DS3_ADDRESS,0x01, 0x80);
  periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_SM_STEP_THS, 0x08);
  periphWrite8(LSM6DS3_ADDRESS,0x01, 0x00);
  // вкл акселерометн (уже включен) 0x20 = 26 Hz
  //periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_CTRL1_XL, 0x20);
  // ВЫКЛЮЧАЮ счетчик шагов
  //periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_TAP_CFG1, 0x00);

  //Включаю SignificantMotion 05
  periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_CTRL10_C, 0x3D);
  // ВКЛючаю счетчик шагов
  //periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_TAP_CFG1, 0x40);
  // Включение 2 остальных датчиков
  //periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_TAP_CFG1, 0x51);
  //periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_INT1_CTRL, 0xC0);  
  periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_INT1_CTRL, 0x40);  
  
  //periphWrite8(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_FUNC_SRC, 0x00);
}

/**
  * @brief Чтение регистра датчика свободного падения
  * @retval Вернет 1 при срабатывании
  */
uint8_t lsm6ds3_FreefallDetectionRead(void)
{
  uint8_t data = 0;
  data = periphReadByte(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_WAKE_UP_SRC);
  data &= 0x20;
  if (data)
    return 1;
  return 0;
}

/**
  * @brief Обнаружение изменение ориентации
  * @retval Вернет 1 при срабатывании
  */
uint8_t lsm6ds3_OrientDetectionRead(void)
{
  static uint8_t current_data = 0;
  uint8_t data = 0;
  data = periphReadByte(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_D6D_SRC);
  data &= 0x3F;
  //data &= 0x40; //D6D_IA плохо работает
  if (current_data == 0)
  {
    current_data = data;
  }
  if (current_data != data)
  {
    current_data = data;
    return 1;
  }
  return 0;
}

/**
  * @brief Чтение регистра датчика свободного падения
  * @retval Вернет 1 при срабатывании
  */
uint8_t lsm6ds3_SignificantMotionDetectionRead(void)
{
  uint8_t data = 0;
  data = periphReadByte(LSM6DS3_ADDRESS,LSM6DS3_ACC_GYRO_FUNC_SRC);
  data &= 0x40;
  if (data)
    return 1;
  return 0;
}

void lsm6ds3_Read(void)
{
  //serialPrint("[lsm6ds3] turning on sensor...\n");
  
}

