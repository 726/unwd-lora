#include "bme280.h"
#include "periph.h"

/**
  * @brief  Инициализация модуля bme280
  * @retval 1 инициализированно успешно
  */
uint8_t bme280_Init(void)
{
  uint8_t id;
  id = periphReadByte(BME280_ADDRESS2, BME280_ID_ADDR);
  for (int i = 0; i < 20000; i++);
  if (id != 0x60){
    return 0;  
  }
  // reset bme
  //periphWrite8(BME280_ADDRESS2, 0xE0, 0xB6);
  // read status
/*
  uint8_t read_byte;
  while (1)
  {
    read_byte = periphReadByte(BME280_ADDRESS2, 0xF3);
    for (int i = 0; i < 1000; i++);
    if ((read_byte & (1 << 0)) != 0)
      break;
  }
*/
  // Ждем готовности NVIC 
  for (int i = 0; i < 20000; i++);
  bme280_Calib();

  // Hum sampling = 1
  periphWrite8(BME280_ADDRESS2, BME280_CTRL_HUM_ADDR, 1);
  // Select control measurement register
  // Normal mode, temp and pressure over sampling rate = 1
  periphWrite8(BME280_ADDRESS2, BME280_CTRL_MEAS_ADDR, 0x27);


  // Select config register
  // Stand_by time 1000ms
  periphWrite8(BME280_ADDRESS2, BME280_CONFIG_ADDR, 0xA0);

  //for (int i = 0; i < 20000; i++);

  return 1;
}

/**
  * @brief  Калибровка модуля bme280
  */
void bme280_Calib(void)
{
  uint8_t buff[24];
  periphRead(BME280_ADDRESS2, BME280_TEMP_DIG_ADDR,buff,24);

  //temp
  bme_calib.dig_T1 = (uint16_t)(buff[1] << 8) + (buff[0]);
  bme_calib.dig_T2 = (int16_t)(buff[3] << 8) + (buff[2]);
  bme_calib.dig_T3 = (int16_t)(buff[5] << 8) + (buff[4]);

  //pressure
  bme_calib.dig_P1 = (uint16_t)(buff[7] << 8) + (buff[6]);
  bme_calib.dig_P2 = (int16_t)(buff[9] << 8) + (buff[8]);
  bme_calib.dig_P3 = (int16_t)(buff[11] << 8) + (buff[10]);
  bme_calib.dig_P4 = (int16_t)(buff[13] << 8) + (buff[12]);
  bme_calib.dig_P5 = (int16_t)(buff[15] << 8) + (buff[14]);
  bme_calib.dig_P6 = (int16_t)(buff[17] << 8) + (buff[16]);
  bme_calib.dig_P7 = (int16_t)(buff[19] << 8) + (buff[18]);
  bme_calib.dig_P8 = (int16_t)(buff[21] << 8) + (buff[20]);
  bme_calib.dig_P9 = (int16_t)(buff[23] << 8) + (buff[22]);

  buff[0] = periphReadByte(BME280_ADDRESS2,BME280_HUM_DIG_ADDR1);
  bme_calib.dig_H1 = (uint8_t)buff[0];

  periphRead(BME280_ADDRESS2,BME280_HUM_DIG_ADDR2,buff,7);
  bme_calib.dig_H2 = (int16_t)((buff[1] << 8) + (buff[0])); //0xE2 + 0xE1
  bme_calib.dig_H3 = (uint8_t)(buff[2]); //0xE3

  bme_calib.dig_H4 = (int16_t)(((int8_t)buff[3] << 4) + (buff[4] & 0x0F)); //0xE4 + 0xE5
  bme_calib.dig_H5 = (int16_t)(((int8_t)buff[5] << 4) + ((buff[4] >> 4) & 0x0F)); //0xE6 + 0xE5
  bme_calib.dig_H6 = (int8_t)(buff[6]);

}

/**
  * @brief  Получение данный температуры, давления и влажности
  */
void bme280_Read(int32_t *temp, uint32_t *pres, uint32_t *humi)
{
  int32_t t_fine = 0;
  //Temperature 27.4 C, humidity: 23.9%, pressure: 1012 mbar
  //Temperature 26.3 C, pressure: 983 mbar, humidity: 36.46%
  *temp = bme280_ReadTemp(&t_fine);
  *pres = bme280_ReadPres(&t_fine);
  *humi = bme280_ReadHum(&t_fine);
  /*
  [bme280] temp value = 22|64.0000
  [bme280] pres value = 992|47.5469
  [bme280] humi value = 37|340.0000
  */
}

int32_t bme280_ReadTemp(int32_t *t_fine)
{
  // Returns temperature in DegC, resolution is 0.01 DegC. Output value of “5123” equals 51.23 DegC.
  uint8_t buff[3];
  //read temp data
  periphRead(BME280_ADDRESS2, BME280_TEMP_ADDR, buff, 3);
  int32_t adc_T = ((uint32_t)buff[0] << 12) | ((uint32_t)buff[1] << 4) | ((buff[2] >> 4) & 0x0F);
  int64_t var1, var2;
  var1 = ((((adc_T>>3) - ((int32_t)bme_calib.dig_T1<<1))) * ((int32_t)bme_calib.dig_T2)) >> 11;
  var2 = (((((adc_T>>4) - ((int32_t)bme_calib.dig_T1)) * ((adc_T>>4) - ((int32_t)bme_calib.dig_T1))) >> 12) *
    ((int32_t)bme_calib.dig_T3)) >> 14;
  *t_fine = var1 + var2;
  int32_t output = ((*t_fine) * 5 + 128) >> 8;
  return output;
}

uint32_t bme280_ReadPres(int32_t *t_fine)
{
  // Returns pressure in Pa as unsigned 32 bit integer in Q24.8 format (24 integer bits and 8 fractional bits).
  // Output value of “24674867” represents 24674867/256 = 96386.2 Pa = 963.862 hPa
  uint8_t buff[3];
  periphRead(BME280_ADDRESS2, BME280_PRESS_ADDR, buff, 3);
  int32_t adc_P = ((uint32_t)buff[0] << 12) | ((uint32_t)buff[1] << 4) | ((buff[2] >> 4) & 0x0F);
  
  int64_t var1, var2, p_acc;
  var1 = ((int64_t)(*t_fine)) - 128000;
  var2 = var1 * var1 * (int64_t)bme_calib.dig_P6;
  var2 = var2 + ((var1 * (int64_t)bme_calib.dig_P5)<<17);
  var2 = var2 + (((int64_t)bme_calib.dig_P4)<<35);
  var1 = ((var1 * var1 * (int64_t)bme_calib.dig_P3)>>8) + ((var1 * (int64_t)bme_calib.dig_P2)<<12);
  var1 = (((((int64_t)1)<<47)+var1))*((int64_t)bme_calib.dig_P1)>>33;
  if (var1 == 0)
  {
    return 0; // avoid exception caused by division by zero
  }
  p_acc = 1048576 - adc_P;
  p_acc = (((p_acc<<31) - var2)*3125)/var1;
  var1 = (((int64_t)bme_calib.dig_P9) * (p_acc>>13) * (p_acc>>13)) >> 25;
  var2 = (((int64_t)bme_calib.dig_P8) * p_acc) >> 19;
  p_acc = ((p_acc + var1 + var2) >> 8) + (((int64_t)bme_calib.dig_P7)<<4);
  
  return (uint32_t)p_acc / 256.0;
}

uint32_t bme280_ReadHum(int32_t *t_fine)
{
  // Returns humidity in %RH as unsigned 32 bit integer in Q22. 10 format (22 integer and 10 fractional bits).
  // Output value of “47445” represents 47445/1024 = 46. 333 %RH
  uint8_t buff[2];
  periphRead(BME280_ADDRESS2, BME280_HUM_ADDR, buff, 2);

  int32_t adc_H = ((uint32_t)buff[0] << 8) | ((uint32_t)buff[1]);

  int32_t var1;

  var1 = ((*t_fine) - ((int32_t)76800));

  var1 = (((((adc_H << 14) - (((int32_t)bme_calib.dig_H4) << 20) - (((int32_t)bme_calib.dig_H5) * var1)) +
    ((int32_t)16384)) >> 15) * (((((((var1 * ((int32_t)bme_calib.dig_H6)) >> 10) * (((var1 * ((int32_t)bme_calib.dig_H3)) >> 11) + ((int32_t)32768))) >> 10) + ((int32_t)2097152)) *
    ((int32_t)bme_calib.dig_H2) + 8192) >> 14));

  var1 = (var1 - (((((var1 >> 15) * (var1 >> 15)) >> 7) * ((int32_t)bme_calib.dig_H1)) >> 4));

  var1 = (var1 < 0 ? 0 : var1);
  var1 = (var1 > 419430400 ? 419430400 : var1);
  return (uint32_t)(var1>>12);
}


