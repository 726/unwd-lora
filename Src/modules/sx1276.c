#include "sx1276.h"

typedef struct
{
  volatile uint8_t addr[8];
  volatile uint8_t bme280;
  volatile uint8_t humi[4];
  volatile uint8_t pres[4];
  volatile uint8_t temp[4];
  volatile uint8_t opt3001;
  volatile uint8_t opt3001_data[4];
  volatile uint8_t lsm6ds3;
  volatile uint8_t lsm6ds3_data;
}dataSend_t;

static volatile uint8_t dataSendBuff[64] = {0};
static dataSend_t* dataPtr = (dataSend_t*) dataSendBuff;

void packet_CleanStruct(void)
{
  for(uint8_t i = 8; i < 64; i++)
  {
    *(dataSendBuff + i) = 0;
  }
}

void packet_SetAddr(void)
{
  uint64_t eeprom_addr;
 
  EEPROM_Unlock();
  eeprom_addr = EEPROM_Read(0x08080000);
  EEPROM_Lock();

  uint8_t *p = (uint8_t *)&eeprom_addr;

  for(uint8_t i = 0; i < 8; i++) 
  {
      dataPtr->addr[i] = p[7 - i];
  }
}

void packet_SetBME280(uint32_t temp, uint32_t pres, uint32_t humi)
{
  dataPtr->bme280 = 0x00;
  for(uint8_t i = 0; i < 4; i++)
  {
    dataPtr->temp[i] = (temp >> 8 * (3 - i)) & 0xFF;
  }
  for(uint8_t i = 0; i < 4; i++)
  {
    dataPtr->pres[i] = (pres >> 8 * (3 - i)) & 0xFF;
  }
  for(uint8_t i = 0; i < 4; i++)
  {
    dataPtr->humi[i] = (humi >> 8 * (3 - i)) & 0xFF;
  }
}

void packet_SetOPT3001(uint32_t lux)
{
  dataPtr->opt3001 = 0x01;
  for(uint8_t i = 0; i < 4; i++)
  {
    dataPtr->opt3001_data[i] = (lux >> 8 * (3 - i)) & 0xFF;
  }   
}

void packet_SetLSM6DS3(uint8_t state)
{
  dataPtr->lsm6ds3 = 0x02;
  dataPtr->lsm6ds3_data = state;
}

void packet_Debug1(void)
{
  serialPrint("\nDebug1:\n");
  for(uint8_t i = 0; i < 28; i++)
  {
    if (*((uint8_t *) dataSendBuff + i) == 0x00)
    {
      serialPrint("00");
    }
    else if ((*((uint8_t *) dataSendBuff + i) & 0xF0) == 0x00)
    {
      serialPrint("0%X", *((uint8_t *) dataSendBuff + i));
    }
    else
    {
      serialPrint("%X", *((uint8_t *) dataSendBuff + i));
    }
  }  
  serialPrint("\n*********************\n");
}

void packet_Debug2(void)
{
  serialPrint("\nDebug2:\n");

  serialPrint("addr: ");
  for(uint8_t i = 0; i < 8; i++)
  {
    if (dataPtr->addr[i] == 0)
    {
      serialPrint("00");
    }
    else if ((dataPtr->addr[i] & 0xF0) == 0)
    {
      serialPrint("0%X", dataPtr->addr[i]);
    }
    else
    {
      serialPrint("%X", dataPtr->addr[i]);
    } 
  }
  serialPrint("\n");


  serialPrint("[bme280] humi: ");
  for(uint8_t i = 0; i < 4; i++)
  {
    if (dataPtr->humi[i] == 0)
    {
      serialPrint("00");
    }
    else if ((dataPtr->humi[i] & 0xF0) == 0)
    {
      serialPrint("0%X", dataPtr->humi[i]);
    }
    else
    {
      serialPrint("%X", dataPtr->humi[i]);
    }
  }
  serialPrint("\n");


  serialPrint("[bme280] pres: ");
  for(uint8_t i = 0; i < 4; i++)
  {
    if (dataPtr->pres[i] == 0)
    {
      serialPrint("00");
    }
    else if ((dataPtr->pres[i] & 0xF0) == 0)
    {
      serialPrint("0%X", dataPtr->pres[i]);
    }
    else
    {
      serialPrint("%X", dataPtr->pres[i]);
    }
  }
  serialPrint("\n");


  serialPrint("[bme280] temp: ");
  for(uint8_t i = 0; i < 4; i++)
  {
    if (dataPtr->temp[i] == 0)
    {
      serialPrint("00");
    }
    else if ((dataPtr->temp[i] & 0xF0) == 0)
    {
      serialPrint("0%X", dataPtr->temp[i]);
    }
    else
    {
      serialPrint("%X", dataPtr->temp[i]);
    }
  }
  serialPrint("\n");


  serialPrint("[opt3001] lux: ");
  for(uint8_t i = 0; i < 4; i++)
  {
    if (dataPtr->opt3001_data[i] == 0)
    {
      serialPrint("00");
    }
    else if ((dataPtr->opt3001_data[i] & 0xF0) == 0)
    {
      serialPrint("0%X", dataPtr->opt3001_data[i]);
    }
    else
    {
      serialPrint("%X", dataPtr->opt3001_data[i]);
    }
  }
  serialPrint("\n");

  serialPrint("[lsm6ds3] state: %X\n", dataPtr->lsm6ds3_data);
  serialPrint("\n*********************\n");
}

void packet_Debug3(void)
{
  serialPrint("\nDebug3:\n");
  for(uint8_t i = 0; i < 28; i++)
  {
    if (*((uint8_t *) dataSendBuff + i) == 0x00)
    {
      serialPrint("data[%u]: 00\n", i);
    }
    else if ((*((uint8_t *) dataSendBuff + i) & 0xF0) == 0x00)
    {
      serialPrint("data[%u]: 0%X\n", i, *((uint8_t *) dataSendBuff + i));
    }
    else
    {
      serialPrint("data[%u]: %X\n", i, *((uint8_t *) dataSendBuff + i));
    }
    
  }  
  serialPrint("\n*********************\n");
}

static void SX1276_CalibrateRxChain(void);

static void SX1276_CalibrateRxChain(void)
{


}

void SX1276_SetHFMode(void)
{
  uint8_t temp = SPI2_ReadByte(SX127X_REG_LR_OPMODE);
  SPI2_WriteByte(SX127X_REG_LR_OPMODE, (temp & SX127X_RF_LORA_OPMODE_FREQMODE_ACCESS_MASK) | SX127X_RF_LORA_OPMODE_FREQMODE_ACCESS_HF);  
}

void SX1276_SetFrequency(uint32_t freq)
{
  uint32_t temp = (freq / 32E6 * (1 << 19));
  SPI2_WriteByte(SX127X_REG_LR_FRFMSB, (uint8_t) ((temp >> 16) & 0xFF));
  SPI2_WriteByte(SX127X_REG_LR_FRFMID, (uint8_t) ((temp >> 8) & 0xFF));
  SPI2_WriteByte(SX127X_REG_LR_FRFLSB, (uint8_t) ((temp >> 0) & 0xFF));
}

void SX1278_CheckChannel(uint32_t freq, uint16_t rssi_thresh)
{

}

void SX1276_Init(void)
{
  SPI2_Init();
  SX1276_RFSwitchInit();
  SX1276_Reset();
  /*SX1276_CalibrateRxChain();
  SX1276_Sleep();
  SX1276_SetHFMode();
  SX1276_SetModeLora();
  SX1276_SetFrequency(868E6);
  */

  SPI2_WriteByte(0x01, 0x00);//sleep
  serialPrint("(SX1276_Config) sleep: %X\n", SPI2_ReadByte(0x01));

  SPI2_WriteByte(0x01, 0x80);//lora
  serialPrint("(SX1276_Config) lora: %X\n", SPI2_ReadByte(0x01));

  SPI2_WriteByte(0x06, 0xd9);//freq == 868E6
  serialPrint("(SX1276_Config) freq: %X\n", SPI2_ReadByte(0x06));

  SPI2_WriteByte(0x07, 0x00);
  serialPrint("(SX1276_Config) freq: %X\n", SPI2_ReadByte(0x07));

  SPI2_WriteByte(0x08, 0x00);
  serialPrint("(SX1276_Config) freq: %X\n", SPI2_ReadByte(0x08));

  SPI2_WriteByte(0x09, 0xFC);//PA=17dB
  serialPrint("(SX1276_Config) PA: %X\n", SPI2_ReadByte(0x09));

  SPI2_WriteByte(0x0B, 0x0B);//off ocp
  serialPrint("(SX1276_Config) ocp: %X\n", SPI2_ReadByte(0x0B));

  SPI2_WriteByte(0x0C, 0x23);//gain max
  for(uint32_t i = 0; i < 10000; i++);
  serialPrint("(SX1276_Config) gain: %X\n", SPI2_ReadByte(0x0C));//err

  SPI2_WriteByte(0x1D, 0x78);//bw7, 4/8, explicit
  serialPrint("(SX1276_Config) bw7: %X\n", SPI2_ReadByte(0x1D));

  SPI2_WriteByte(0x1E, 0xC7); //sf7, normal, crc
  serialPrint("(SX1276_Config) sf7: %X\n", SPI2_ReadByte(0x1E));

  SPI2_WriteByte(0x1F, 0xFF); //max timeout rx
  serialPrint("(SX1276_Config) timeout: %X\n", SPI2_ReadByte(0x1F));
  
  SPI2_WriteByte(0x21, 0x12); //preable12
  serialPrint("(SX1276_Config) preable12: %X\n", SPI2_ReadByte(0x21));
  
  SPI2_WriteByte(0x01, 0x81);//standby 
  for(uint32_t i = 0; i < 10000; i++);
  serialPrint("(SX1276_Config) standby: %X\n", SPI2_ReadByte(0x01));//err0

}

void SX1276_Reset(void)
{
  SX1276_ResetOn(); //push-pull output
  Delay(10000); //10 ms
  SX1276_ResetOff(); //input floating
  Delay(10000); //10 ms
}

void SX1276_Sleep(void)
{
  uint8_t temp = SPI2_ReadByte(SX127X_REG_LR_OPMODE);
  SPI2_WriteByte(SX127X_REG_LR_OPMODE, (temp & SX127X_RF_OPMODE_MASK) | SX127X_RF_OPMODE_SLEEP);
  SX1276_RFSwitchOff();
}

void SX1276_Standby(void)
{
  uint8_t temp = SPI2_ReadByte(SX127X_REG_LR_OPMODE);
  SPI2_WriteByte(SX127X_REG_LR_OPMODE, (temp & SX127X_RF_OPMODE_MASK) | SX127X_RF_OPMODE_STANDBY);
  SX1276_RFSwitchOn();
}

void SX1276_SetModeLora(void)
{
  uint8_t temp = SPI2_ReadByte(SX127X_REG_LR_OPMODE);
  SPI2_WriteByte(SX127X_REG_LR_OPMODE, (temp & SX127X_RF_OPMODE_LONGRANGEMODE_MASK) | SX127X_RF_OPMODE_LONGRANGEMODE_ON);
  Delay(10000); //10 ms
}

uint8_t SX1276_GetRSSI(void)
{
  return ((-157 + SPI2_ReadByte(0x1A)) & 0xFF);
}

void SX1276_ReadFIFO(uint8_t* data, uint32_t len)
{
  SPI2_ReadBurst(0x00, data, len);
}

void SX1276_WriteFIFO(uint8_t* data, uint32_t len)
{
  SPI2_WriteBurst(0x00, data, len);
}

void SX1276_SetRx(void)
{
  serialPrint("Receive1\n");
  SX1276_RFSwitchOn();

  SPI2_WriteByte(0x11, 0x1F);
  serialPrint("(SX1276_Config) irqmask: %X\n", SPI2_ReadByte(0x11));
  /*
  serialPrint("(1) freq: %X\n", SPI2_ReadByte(SX127X_REG_LR_FRFMSB));
  serialPrint("(1) freq: %X\n", SPI2_ReadByte(SX127X_REG_LR_FRFMID));
  serialPrint("(1) freq: %X\n\n", SPI2_ReadByte(SX127X_REG_LR_FRFLSB));

  serialPrint("(1) paconf: %X\n", SPI2_ReadByte(SX127X_REG_LR_PACONFIG));
  SPI2_WriteByte(SX127X_REG_LR_PACONFIG, 0xCF);
  serialPrint("(1) paconf: %X\n\n", SPI2_ReadByte(SX127X_REG_LR_PACONFIG));

  serialPrint("(1) paramp: %X\n", SPI2_ReadByte(SX127X_REG_LR_PARAMP));
  SPI2_WriteByte(SX127X_REG_LR_PARAMP, (SPI2_ReadByte(SX127X_REG_LR_PARAMP) & 0xF0) | 0x08);
  serialPrint("(1) paramp: %X\n\n", SPI2_ReadByte(SX127X_REG_LR_PARAMP));

  serialPrint("(1) conf1: %X\n", SPI2_ReadByte(SX127X_REG_LR_MODEMCONFIG1));
  SPI2_WriteByte(SX127X_REG_LR_MODEMCONFIG1, 0x82);
  serialPrint("(1) conf1: %X\n\n", SPI2_ReadByte(SX127X_REG_LR_MODEMCONFIG1));

  serialPrint("(1) conf2: %X\n", SPI2_ReadByte(SX127X_REG_LR_MODEMCONFIG2));
  SPI2_WriteByte(SX127X_REG_LR_MODEMCONFIG2, 0xC3);
  serialPrint("(1) conf2: %X\n\n", SPI2_ReadByte(SX127X_REG_LR_MODEMCONFIG2));

  serialPrint("(1) sto: %X\n", SPI2_ReadByte(SX127X_REG_LR_SYMBTIMEOUTLSB));
  SPI2_WriteByte(SX127X_REG_LR_SYMBTIMEOUTLSB, 0x05);
  serialPrint("(1) sto: %X\n\n", SPI2_ReadByte(SX127X_REG_LR_SYMBTIMEOUTLSB));

  serialPrint("(1) conf3: %X\n", SPI2_ReadByte(SX127X_REG_LR_MODEMCONFIG3));
  SPI2_WriteByte(SX127X_REG_LR_MODEMCONFIG3, (SPI2_ReadByte(SX127X_REG_LR_MODEMCONFIG3) & 0xF7) | 0x0C);
  serialPrint("(1) conf3: %X\n\n", SPI2_ReadByte(SX127X_REG_LR_MODEMCONFIG3));

  serialPrint("(1) pl: %X\n", SPI2_ReadByte(SX127X_REG_LR_PAYLOADMAXLENGTH));
  SPI2_WriteByte(SX127X_REG_LR_MODEMCONFIG3, 0x40);
  serialPrint("(1) pl: %X\n\n", SPI2_ReadByte(SX127X_REG_LR_PAYLOADMAXLENGTH));

  serialPrint("(1) opt: %X\n", SPI2_ReadByte(SX127X_REG_LR_INVERTIQ));
  SPI2_WriteByte(SX127X_REG_LR_INVERTIQ, 0x67);
  serialPrint("(1) opt: %X\n\n", SPI2_ReadByte(SX127X_REG_LR_INVERTIQ));

  serialPrint("(1) opt: %X\n", SPI2_ReadByte(SX127X_REG_LR_INVERTIQ2));
  SPI2_WriteByte(SX127X_REG_LR_INVERTIQ2, 0x19);
  serialPrint("(1) opt: %X\n\n", SPI2_ReadByte(SX127X_REG_LR_INVERTIQ2));
  */
}

void SX1276_SetTx(void)
{
  serialPrint("TRANSMIT\n");
  SX1276_RFSwitchOn();

  SPI2_WriteByte(0x11, 0xF7);
  serialPrint("(SX1276_Config) irqmask: %X\n", SPI2_ReadByte(0x11));

  /*
  serialPrint("(1) freq: %X\n", SPI2_ReadByte(SX127X_REG_LR_FRFMSB));
  serialPrint("(1) freq: %X\n", SPI2_ReadByte(SX127X_REG_LR_FRFMID));
  serialPrint("(1) freq: %X\n\n", SPI2_ReadByte(SX127X_REG_LR_FRFLSB));

  serialPrint("(1) lna: %X\n", SPI2_ReadByte(SX127X_REG_LR_LNA));
  SPI2_WriteByte(SX127X_REG_LR_LNA, (SPI2_ReadByte(SX127X_REG_LR_LNA) & SX127X_RF_LORA_LNA_GAIN_MASK) | SX127X_RF_LORA_LNA_GAIN_G1 | SX127X_RF_LORA_LNA_BOOST_HF_ON);
  serialPrint("(1) lna: %X\n\n", SPI2_ReadByte(SX127X_REG_LR_LNA));

  serialPrint("(1) conf1: %X\n", SPI2_ReadByte(SX127X_REG_LR_MODEMCONFIG1));
  SPI2_WriteByte(SX127X_REG_LR_MODEMCONFIG1, 0x82);
  serialPrint("(1) conf1: %X\n\n", SPI2_ReadByte(SX127X_REG_LR_MODEMCONFIG1));

  serialPrint("(1) conf2: %X\n", SPI2_ReadByte(SX127X_REG_LR_MODEMCONFIG2));
  SPI2_WriteByte(SX127X_REG_LR_MODEMCONFIG2, 0xC3);
  serialPrint("(1) conf2: %X\n\n", SPI2_ReadByte(SX127X_REG_LR_MODEMCONFIG2));

  serialPrint("(1) conf3: %X\n", SPI2_ReadByte(SX127X_REG_LR_MODEMCONFIG3));
  SPI2_WriteByte(SX127X_REG_LR_MODEMCONFIG3, (SPI2_ReadByte(SX127X_REG_LR_MODEMCONFIG3) & 0xF7) | 0x08);
  serialPrint("(1) conf3: %X\n\n", SPI2_ReadByte(SX127X_REG_LR_MODEMCONFIG3));

  SPI2_WriteByte(0x11, 0xF7);
  serialPrint("(SX1276_Config) irqmask: %X\n", SPI2_ReadByte(0x11));
  */
}
void SX1276_Transmit(void)//uint8_t* data, uint32_t len)
{ 

  SPI2_WriteByte(0x22, 0x1C);
  serialPrint("(SX1276_Config) payload: %X\n", SPI2_ReadByte(0x22));

  SPI2_WriteByte(0x0D, SPI2_ReadByte(0x0E));
  serialPrint("(SX1276_Config) fifo: %X\n", SPI2_ReadByte(0x0D));

  SX1276_WriteFIFO(dataSendBuff, 28);
  SPI2_WriteByte(0x01, 0x83);    //Tx Mode
  while(1)
  {
    if (SPI2_ReadByte(0x12) & 0x08)
    {
      serialPrint("(SX1276_Config) irq: %X\n", SPI2_ReadByte(0x12));
      SPI2_WriteByte(0x12, 0xFF);
      serialPrint("(SX1276_Config) irqoff: %X\n", SPI2_ReadByte(0x12));
      //SPI2_WriteByte(0x01, 0x81);
      serialPrint("(SX1276_Config) mode: %X\n", SPI2_ReadByte(0x01));
      packet_CleanStruct();
      break;
    }
  }
}

void SX1276_Recieve(uint8_t* buff, uint32_t* len)
{
  SPI2_WriteByte(0x01, 0x85);
  uint8_t irq;
  while(1)
  {
    irq = SPI2_ReadByte(0x12);
    if (irq & 0x20)
    {
      serialPrint("\n(SX1276_Config) irq: %X\n", irq);

      SPI2_WriteByte(0x12, 0xFF);
      serialPrint("(SX1276_Config) irqoff: %X\n", SPI2_ReadByte(0x12));

      SPI2_WriteByte(0x0D,  SPI2_ReadByte(0x10));
      serialPrint("(SX1276_Config) fifo: %X\n", SPI2_ReadByte(0x0D));

      serialPrint("ERR");
      //buff[0] = 0xAA;
      //buff[1] = 0xBB;
      //break;

    }
    else if (irq & 0xC0)
    {
      serialPrint("(SX1276_Config) irq: %X\n", irq);

      SPI2_WriteByte(0x12, 0xFF);
      serialPrint("(SX1276_Config) irqoff: %X\n", SPI2_ReadByte(0x12));

      SPI2_WriteByte(0x0D,  SPI2_ReadByte(0x10));
      serialPrint("(SX1276_Config) fifo: %X\n", SPI2_ReadByte(0x0D));

      SX1276_ReadFIFO(buff, SPI2_ReadByte(0x13));
      serialPrint("(SX1276_Config) len: %X\n", SPI2_ReadByte(0x13));
      *len = SPI2_ReadByte(0x13);
      GPIOB->ODR ^= GPIO_ODR_ODR_0;
      break;
    } 
  }
}

void SX1276_DI0IRQ(void)
{

}

void SX1276_DI1IRQ(void)
{
  
}

void SX1276_DI2IRQ(void)
{

}

void SX1276_DI3IRQ(void)
{

}

void SX1276_DI4IRQ(void)
{

}