#include "periph.h"

/**
  * @brief  Инициализация i2c для работы с периферией
  */
void periphInit(void) 
{
	I2C2_Init();
}

/**
  * @brief  i2c Чтение из адреса регистра ведомого устройства
  * @param  slave_addr  - адрес ведомого устройства
  * @param  slave_reg   - регистр ведомого устройства
  * @param  *buf        - буфер считаных данных
  * @param  bytes_count - количество байт на чтение
  */
void periphRead (uint8_t slave_addr, uint8_t slave_reg, uint8_t *buf, uint16_t bytes_count)
{
  I2C2_Read(slave_addr, slave_reg, buf, bytes_count);
}

/**
  * @brief  i2c Чтение одного байта из адреса регистра ведомого устройства
  * @param  slave_addr  - адрес ведомого устройства
  * @param  slave_reg   - регистр ведомого устройства
  */
uint8_t periphReadByte (uint8_t slave_addr, uint8_t slave_reg)
{
  uint8_t buf[1];
  I2C2_Read(slave_addr,slave_reg,buf,1);
  return buf[0];
}

/**
  * @brief  i2c Запись в адрес регистра ведомого устройства
  * @param  slave_addr  - адрес ведомого устройства
  * @param  slave_reg   - регистр ведомого устройства
  * uint8_t *buf, uint16_t bytes_count
  */
void periphWrite16 (uint8_t slave_addr, uint8_t slave_reg, uint16_t bytes_write)
{
  I2C2_Write16(slave_addr, slave_reg, bytes_write);
}

/**
  * @brief  i2c Запись в адрес регистра ведомого устройства
  * @param  slave_addr  - адрес ведомого устройства
  * @param  slave_reg   - регистр ведомого устройства
  */
void periphWrite8 (uint8_t slave_addr, uint8_t slave_reg, uint8_t bytes_write)
{
  I2C2_Write8(slave_addr, slave_reg, bytes_write);
}
