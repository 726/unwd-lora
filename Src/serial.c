#include "serial.h"

/**
  * @brief  Инициализация Serial
  */
void serialInit(void) 
{
	UART1_Init();	
}

/**
  * @brief  Простая реализация printf
  * @param  msg Строка для печати
  */
void serialPrint(char *msg, ...)
{
	char buff[120];
	va_list args;
	va_start(args,msg); 
	vsprintf(buff,msg,args);

	for(int i=0;i<strlen(buff);i++)
	{
		UART_Write(USART1, buff[i]);
	}
}

/**
  * @brief  Костыль для UART2
  * @param  msg Строка для печати
  */
void serialPrint2(char *msg, ...)
{
	char buff[120];
	va_list args;
	va_start(args,msg); 
	vsprintf(buff,msg,args);

	for(int i=0;i<strlen(buff);i++)
	{
		UART_Write(USART2, buff[i]);
	}
}

/**
  * @brief  Считывает пришедший байт
  * @retval uint8_t возвращает пришедший байт
  */
uint8_t serialReadByte(void) 
{
	uint8_t byte;
	byte = UART_Read(USART1);
	return byte;
}
