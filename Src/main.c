/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @author agentgosdepa, akeela
  * @author 
  *
  ******************************************************************************
  */

#include "main.h"

void SystemClock_Config(void);

int main(void)
{
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_COMP);
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_SYSCFG);
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);
  NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
  SystemClock_Config();

  Led_Init();
  serialInit();
  periphInit();
  SX1276_Init();
  UART2_Init();



  //-------------------------------------------
  /**/
  if (1)
  {
    packet_CleanStruct();
    packet_SetAddr();
    SX1276_SetTx();

    uint32_t temp = 0xCC;
    uint32_t pres = 0xBB;
    uint32_t humi = 0xAA;
    uint32_t lux = 0x99;  
    uint8_t state = 0;
    uint8_t data_freefall = 0;
    uint8_t data_sign = 0;
    uint8_t data_orient = 0;

    //opt3001_Init();
    //bme280_Init();
    lsm6ds3_Init();
    lsm6ds3_FreefallDetectionEnable();
    lsm6ds3_SignificantMotionDetectionEnable();
    lsm6ds3_OrientDetectionEnable();

    while(1)
    {
      //opt3001_Read(&lux);
      //bme280_Read(&temp, &pres, &humi);

      data_freefall = lsm6ds3_FreefallDetectionRead();
      data_sign = lsm6ds3_SignificantMotionDetectionRead();
      data_orient = lsm6ds3_OrientDetectionRead();
      if (data_freefall)
      {
        //serialPrint("[lsm6ds3] Freefall\n");
        state = 1;
        for(volatile uint32_t i = 0; i < 50000; i++);
      }
      if (data_sign)
      {
        //serialPrint("[lsm6ds3] Significant motion\n");
        state = 2;
        for(volatile uint32_t i = 0; i < 50000; i++);
      }
      if (data_orient)
      {
        //serialPrint("[lsm6ds3] Orient\n");
        state = 4;
        for(volatile uint32_t i = 0; i < 50000; i++);
      }


      packet_SetBME280(temp, pres, humi);
      packet_SetOPT3001(lux);
      packet_SetLSM6DS3(state);
      packet_Debug1();
      packet_Debug2();
      packet_Debug3();

      /*
      //serialPrint("[bme280] temp value = %u\n", (int32_t) temp);
      serialPrint("[bme280] temp value = %X\n", temp);
      //serialPrint("[bme280] pres value = %u\n", pres);
      serialPrint("[bme280] pres value = %X\n", pres);
      //serialPrint("[bme280] humi value = %u\n", humi);
      serialPrint("[bme280] humi value = %X\n", humi);
      //serialPrint("[opt3001] lux value = %u\n", lux);
      serialPrint("[opt3001] lux value = %X\n", lux);
      */

      for(volatile uint32_t i = 0; i < 80000; i++);
      Led_ChangeState();

      SX1276_Transmit();
      for(volatile uint32_t i = 0; i < 50000; i++);
    }
  }
  else
  {
    uint8_t buffrecv[64] = {0};
    uint32_t* buffrecvlen = 0;
    SX1276_SetRx();
    while(1)
    {
      SX1276_Recieve(buffrecv, buffrecvlen);

      serialPrint("\nDebug3:\n");
      for(uint32_t i = 0; i < 28; i++)
      {
        if (i == 8)
        {
          if (1)
          {
            uint8_t rssi = SX1276_GetRSSI();
            if (rssi == 0x00)
            {
              serialPrint("00");
            }
            else if ((rssi & 0xF0) == 0x00)
            {
              serialPrint("0%X", rssi);
            }
            else
            {
              serialPrint("%X", rssi);
            }
            serialPrint2("%X", rssi);
          }

          serialPrint("%X", 0x14);
          serialPrint2("%X", 0x14);
        }

        if (buffrecv[i] == 0x00)
        {
          serialPrint("00");
        }
        else if ((buffrecv[i] & 0xF0) == 0x00)
        {
          serialPrint("0%X", buffrecv[i]);
        }
        else
        {
          serialPrint("%X", buffrecv[i]);
        }

        serialPrint2("%X", buffrecv[i]);
      }

      for(uint32_t i=0; i<64; i++)
      {
        buffrecv[i] = 0;
      }
    }
  }
  /**/

  while(1)
  {

  }

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  LL_FLASH_Enable64bitAccess();

  LL_FLASH_SetLatency(LL_FLASH_LATENCY_0);

  if(LL_FLASH_GetLatency() != LL_FLASH_LATENCY_0)
  {
  Error_Handler();  
  }
  LL_PWR_SetRegulVoltageScaling(LL_PWR_REGU_VOLTAGE_SCALE1);
  LL_RCC_MSI_Enable();

   /* Wait till MSI is ready */
  while(LL_RCC_MSI_IsReady() != 1)
  {
    
  }
  LL_RCC_MSI_SetRange(LL_RCC_MSIRANGE_5);
  LL_RCC_MSI_SetCalibTrimming(0);
  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
  LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_MSI);

   /* Wait till System clock is ready */
  while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_MSI)
  {
  
  }
  LL_Init1msTick(2097000);
  LL_SYSTICK_SetClkSource(LL_SYSTICK_CLKSOURCE_HCLK);
  LL_SetSystemCoreClock(2097000);
}


/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* User can add his own implementation to report the HAL error return state */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
}
#endif /* USE_FULL_ASSERT */

/*****END OF FILE****/