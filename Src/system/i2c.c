#include "i2c.h"

/**
  * @brief  Инициализация i2c для работы с периферией
  */
void I2C2_Init(void)
{
    LL_I2C_InitTypeDef I2C_InitStruct = {0};
    LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);

    /**I2C2 GPIO Configuration  
    PB10   ------> I2C2_SCL
    PB11   ------> I2C2_SDA 
    */
  
    GPIO_InitStruct.Pin = LL_GPIO_PIN_10 | LL_GPIO_PIN_11;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
    GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_OPENDRAIN;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
    GPIO_InitStruct.Alternate = LL_GPIO_AF_4;
    LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* Peripheral clock enable */
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_I2C2);

    LL_I2C_EnableClockStretching(I2C2);
    I2C_InitStruct.PeripheralMode = LL_I2C_MODE_I2C;
    I2C_InitStruct.ClockSpeed = 100000;
    I2C_InitStruct.DutyCycle = LL_I2C_DUTYCYCLE_2;
    I2C_InitStruct.TypeAcknowledge = LL_I2C_ACK;
    I2C_InitStruct.OwnAddrSize = LL_I2C_OWNADDRESS1_7BIT;
    LL_I2C_Init(I2C2, &I2C_InitStruct);
    LL_I2C_DisableOwnAddress2(I2C2);
    LL_I2C_DisableGeneralCall(I2C2);
    

    //I2C2->CR2 |= I2C_CR2_ITEVTEN;
    //I2C2->CR1 |= I2C_CR1_PE;            // Enable I2C block
    //NVIC_EnableIRQ(I2C2_EV_IRQn);
    //NVIC_SetPriority(I2C2_EV_IRQn, 1);
    

    //HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
    //HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
    
    //NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
    //NVIC_EnableIRQ(EXTI15_10_IRQn);

    //interrupts for lsm6ds3
    /* I2C2_EV_IRQn interrupt configuration */
  //NVIC_SetPriority(I2C2_EV_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 3));
  //NVIC_EnableIRQ(I2C2_EV_IRQn);
  /* I2C2_ER_IRQn interrupt configuration */
  //NVIC_SetPriority(I2C2_ER_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 2));
  //NVIC_EnableIRQ(I2C2_ER_IRQn);

}


/**
  * @brief  i2c Чтение из адреса регистра ведомого устройства
  * @param  slave_addr  - адрес ведомого устройства
  * @param  slave_reg   - регистр ведомого устройства
  * @param  *buf        - буфер считаных данных
  * @param  bytes_count - количество байт на чтение
  */
void I2C2_Read (uint8_t slave_addr, uint8_t slave_reg, uint8_t *buf, uint16_t bytes_count)
{
  uint16_t i;
  LL_I2C_DisableBitPOS(I2C2);
  LL_I2C_AcknowledgeNextData(I2C2, LL_I2C_ACK);
  LL_I2C_GenerateStartCondition(I2C2);
  while (!(I2C2->SR1 & I2C_SR1_SB)){};
  (void) I2C2->SR1;

  LL_I2C_TransmitData8(I2C2, (slave_addr << 1) | I2C_REQUEST_WRITE);
  while (!(I2C2->SR1 & I2C_SR1_ADDR)){};
  (void) I2C2->SR1;
  (void) I2C2->SR2;

  LL_I2C_TransmitData8(I2C2, slave_reg);
  while(!LL_I2C_IsActiveFlag_TXE(I2C2)){};

  LL_I2C_GenerateStartCondition(I2C2);
  while(!LL_I2C_IsActiveFlag_SB(I2C2)){};
  (void) I2C2->SR1;

  LL_I2C_TransmitData8(I2C2, (slave_addr << 1) | I2C_REQUEST_READ);
  while (!LL_I2C_IsActiveFlag_ADDR(I2C2)){};
  LL_I2C_ClearFlag_ADDR(I2C2);
  for(i=0;i<bytes_count;i++)
  {
    if(i<(bytes_count-1))
    {
      while(!LL_I2C_IsActiveFlag_RXNE(I2C2)){};
      buf[i] = LL_I2C_ReceiveData8(I2C2);
    }
    else
    {
      LL_I2C_AcknowledgeNextData(I2C2, LL_I2C_NACK);
      LL_I2C_GenerateStopCondition(I2C2);
      while(!LL_I2C_IsActiveFlag_RXNE(I2C2)){};
      buf[i] = LL_I2C_ReceiveData8(I2C2);
    }
  }
}

/**
  * @brief  i2c Запись в адрес регистра ведомого устройства
  * @param  slave_addr  - адрес ведомого устройства
  * @param  slave_reg   - регистр ведомого устройства
  */
void I2C2_Write16 (uint8_t slave_addr, uint8_t slave_reg, uint16_t bytes_write)
{
  uint16_t i;
  LL_I2C_DisableBitPOS(I2C2);
  LL_I2C_AcknowledgeNextData(I2C2, LL_I2C_ACK);
  LL_I2C_GenerateStartCondition(I2C2);

  while (!(I2C2->SR1 & I2C_SR1_SB)){};
  (void) I2C2->SR1;

  LL_I2C_TransmitData8(I2C2, (slave_addr << 1) | I2C_REQUEST_WRITE);
  while (!(I2C2->SR1 & I2C_SR1_ADDR)){};
  (void) I2C2->SR1;
  (void) I2C2->SR2;

  LL_I2C_TransmitData8(I2C2, slave_reg);
  while(!LL_I2C_IsActiveFlag_TXE(I2C2)){};

  LL_I2C_TransmitData8(I2C2, (uint8_t)(bytes_write >> 8));
  while(!LL_I2C_IsActiveFlag_TXE(I2C2)){};
  LL_I2C_TransmitData8(I2C2, (uint8_t)(bytes_write));
  while(!LL_I2C_IsActiveFlag_TXE(I2C2)){};

  LL_I2C_GenerateStopCondition(I2C2);
}

/**
  * @brief  i2c Запись в адрес регистра ведомого устройства
  * @param  slave_addr  - адрес ведомого устройства
  * @param  slave_reg   - регистр ведомого устройства
  */
void I2C2_Write8 (uint8_t slave_addr, uint8_t slave_reg, uint8_t bytes_write)
{
  uint16_t i;
  LL_I2C_DisableBitPOS(I2C2);
  LL_I2C_AcknowledgeNextData(I2C2, LL_I2C_ACK);
  LL_I2C_GenerateStartCondition(I2C2);

  while (!(I2C2->SR1 & I2C_SR1_SB)){};
  (void) I2C2->SR1;

  LL_I2C_TransmitData8(I2C2, (slave_addr << 1) | I2C_REQUEST_WRITE);
  while (!(I2C2->SR1 & I2C_SR1_ADDR)){};
  (void) I2C2->SR1;
  (void) I2C2->SR2;

  LL_I2C_TransmitData8(I2C2, slave_reg);
  while(!LL_I2C_IsActiveFlag_TXE(I2C2)){};

  //пишем данные  
  LL_I2C_TransmitData8(I2C2, (uint8_t)(bytes_write));
  while(!LL_I2C_IsActiveFlag_TXE(I2C2)){};

  LL_I2C_GenerateStopCondition(I2C2);
}
