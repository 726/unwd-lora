#include "eeprom.h"

uint8_t EEPROM_Unlock(void)
{
	/*
	#define FLASH_PEKEY1               (0x89ABCDEFU) //Flash program erase key1
	#define FLASH_PEKEY2               (0x02030405U)
	*/
	//if((FLASH->PECR & FLASH_PECR_PELOCK) != RESET)
	//{  
	    // Unlocking the Data memory and FLASH_PECR register access
	    FLASH->PEKEYR = FLASH_PEKEY1;
	    FLASH->PEKEYR = FLASH_PEKEY2;
	//}
	//else
	//	return 0;
	return 1;  
}

uint8_t EEPROM_Lock(void)
{
	FLASH->PECR |= FLASH_PECR_PELOCK;
	return 1;
}

uint8_t EEPROM_Erase(uint32_t TypeErase, uint32_t Address)
{
	/*
	#define FLASH_TYPEERASEDATA_BYTE         (0x00U)  // Стереть байт
	#define FLASH_TYPEERASEDATA_HALFWORD     (0x01U)  // Стереть 2 байта
	#define FLASH_TYPEERASEDATA_WORD         (0x02U)  // Стереть 4 байта
	*/

}

uint64_t EEPROM_Read(uint64_t address)
{
	return (*(__IO uint64_t *)address);
}

void EEPROM_Write(uint32_t address, uint32_t value)
{
	/*
	HAL_StatusTypeDef flash_ok = HAL_ERROR;
  while (flash_ok != HAL_OK)
  {
    flash_ok = HAL_FLASHEx_DATAEEPROM_Unlock();
  }
  flash_ok = HAL_ERROR;
  while (flash_ok != HAL_OK)
  {
    flash_ok = HAL_FLASHEx_DATAEEPROM_Erase (FLASH_TYPEERASEDATA_WORD, address);
  }
  flash_ok = HAL_ERROR;
  while (flash_ok != HAL_OK)
  {
    flash_ok = HAL_FLASHEx_DATAEEPROM_Program (FLASH_TYPEPROGRAMDATA_WORD, address, value);
  }
  flash_ok = HAL_ERROR;
  while (flash_ok != HAL_OK)
  {
    flash_ok = HAL_FLASHEx_DATAEEPROM_Lock ();
	*/
}