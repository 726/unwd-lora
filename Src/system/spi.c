#include "spi.h"

void SPI2_Init(void)
{
  LL_SPI_InitTypeDef SPI_InitStruct = {0};

  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* Peripheral clock enable */
  
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);
  /**SPI2 GPIO Configuration  
  PB12   ------> SPI2_NSS
  PB13   ------> SPI2_SCK
  PB14   ------> SPI2_MISO
  PB15   ------> SPI2_MOSI 
  */
  GPIO_InitStruct.Pin = LL_GPIO_PIN_12;
  //GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  //GPIO_InitStruct.Alternate = LL_GPIO_AF_5;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = LL_GPIO_PIN_13 | LL_GPIO_PIN_14 | LL_GPIO_PIN_15;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_5;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);
  
  SPI_InitStruct.TransferDirection = LL_SPI_FULL_DUPLEX;
  SPI_InitStruct.Mode = LL_SPI_MODE_MASTER;
  SPI_InitStruct.DataWidth = LL_SPI_DATAWIDTH_8BIT;
  SPI_InitStruct.ClockPolarity = LL_SPI_POLARITY_LOW;
  SPI_InitStruct.ClockPhase = LL_SPI_PHASE_2EDGE;
  SPI_InitStruct.NSS = LL_SPI_NSS_SOFT;
  SPI_InitStruct.BaudRate = LL_SPI_BAUDRATEPRESCALER_DIV4;
  SPI_InitStruct.BitOrder = LL_SPI_MSB_FIRST;
  SPI_InitStruct.CRCCalculation = LL_SPI_CRCCALCULATION_DISABLE;
  SPI_InitStruct.CRCPoly = 10;

  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_SPI2);

  LL_SPI_Init(SPI2, &SPI_InitStruct);
  LL_SPI_SetStandard(SPI2, LL_SPI_PROTOCOL_MOTOROLA);

  GPIOB->BSRR |= GPIO_BSRR_BS_12;

  //serialPrint("spi init completed \n");
}

uint8_t SPI2_ReadByte(uint8_t reg)
{
  uint8_t outbuf[1];
  SPI2_ReadBurst(reg, outbuf, 1);
  return outbuf[0];
}

void SPI2_WriteByte(uint8_t reg, uint8_t inreg)
{
  uint8_t inbuf[1];
  inbuf[0] = inreg;
  SPI2_WriteBurst(reg, inbuf, 1);
}

void SPI2_ReadBurst(uint8_t reg, uint8_t *outbuf, uint8_t len)
{
  SPI2->CR1 |= (SPI_CR1_SPE);
  GPIOB->BSRR |= GPIO_BSRR_BR_12;

  while(!(SPI2->SR & SPI_SR_TXE));
  *(volatile uint8_t *)&SPI2->DR = (reg);
  while(!(SPI2->SR & SPI_SR_RXNE));
  *(volatile uint8_t *)&(SPI2->DR);

  for(uint32_t i = 0; i < len; i++)
  {
    while(!(SPI2->SR & SPI_SR_TXE));
    *(volatile uint8_t *)&SPI2->DR = 0;
    while(!(SPI2->SR & SPI_SR_RXNE));
    outbuf[i] = *(volatile uint8_t *)&(SPI2->DR);
  }

  while(SPI2->SR & SPI_SR_BSY);

  GPIOB->BSRR |= GPIO_BSRR_BS_12;
  SPI2->CR1 &= ~SPI_CR1_SPE;

}

void SPI2_WriteBurst(uint8_t reg, uint8_t *inbuf, uint8_t len)
{
  SPI2->CR1 |= (SPI_CR1_SPE); //можно не комменттить, ничего не меняется
  GPIOB->BSRR |= GPIO_BSRR_BR_12;

  while(!(SPI2->SR & SPI_SR_TXE));
  *(__IO uint8_t *)&SPI2->DR = (reg | 0x80);
  while(!(SPI2->SR & SPI_SR_RXNE));
  *(volatile uint8_t *)&(SPI2->DR);

  for(uint32_t i = 0; i < len; i++)
  {  
    while(!(SPI2->SR & SPI_SR_TXE));
    *(__IO uint8_t *)&SPI2->DR = inbuf[i];
    while(!(SPI2->SR & SPI_SR_RXNE));
    *(volatile uint8_t *)&(SPI2->DR);
  }

  while(SPI2->SR & SPI_SR_BSY);

  GPIOB->BSRR |= GPIO_BSRR_BS_12;
  SPI2->CR1 &= ~SPI_CR1_SPE;
}
