#include "gpio.h"

void SX1276_RFSwitchInit(void)
{
  GPIOA->MODER |= GPIO_MODER_MODER11_0;
}

void SX1276_RFSwitchOff(void)
{
  GPIOA->BSRR |= GPIO_BSRR_BR_11;
}

void SX1276_RFSwitchOn(void)
{
  GPIOA->BSRR |= GPIO_BSRR_BS_11;
}


void SX1276_ResetOn(void)
{

}

void SX1276_ResetOff(void)
{

}

void SX1276_DI0Init(void)
{

}

void SX1276_DI1Init(void)
{

}

void SX1276_DI2Init(void)
{

}

void SX1276_DI3Init(void)
{

}

void SX1276_DI4Init(void)
{

}

void Led_Init(void)
{
  RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
  GPIOB->MODER &= !GPIO_MODER_MODER0;
  GPIOB->MODER |= GPIO_MODER_MODER0_0;
  GPIOB->BSRR |= GPIO_BSRR_BR_0;
}

void Led_On(void)
{
  GPIOB->BSRR |= GPIO_BSRR_BS_0;
}

void Led_Off(void)
{
  GPIOB->BSRR |= GPIO_BSRR_BR_0;
}

void Led_ChangeState(void)
{
  GPIOB->ODR ^= GPIO_ODR_ODR_0;
}
