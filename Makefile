# ------------------------------------------------
# Generic Makefile (based on gcc)
#
# ChangeLog :
#	2019-11-09 - Added firmware flash tool
# ------------------------------------------------

TARGET = unwd_lora

# debug build?
DEBUG = 1
# optimization
OPT = -Og

# Build path
BUILD_DIR = build

# C sources
C_SOURCES =  \
Src/main.c \
Src/stm32l1xx_it.c \
Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_rcc.c \
Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_utils.c \
Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_exti.c \
Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_gpio.c \
Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_pwr.c \
Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_usart.c \
Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_i2c.c \
Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_spi.c \
Drivers/STM32L1xx_HAL_Driver/Src/stm32l1xx_ll_dma.c \
Src/system_stm32l1xx.c \
Src/system/spi.c \
Src/system/i2c.c \
Src/system/uart.c \
Src/system/gpio.c \
Src/system/eeprom.c \
Src/serial.c \
Src/periph.c \
Src/timer.c \
Src/modules/opt3001.c \
Src/modules/bme280.c \
Src/modules/lsm6ds3.c \
Src/modules/sx1276.c \
Src/crypto/aes.c


# ASM sources
ASM_SOURCES =  \
startup_stm32l151xc.s


# binaries
PREFIX = arm-none-eabi-
# The gcc compiler bin path can be either defined in make command via GCC_PATH variable (> make GCC_PATH=xxx)
# either it can be added to the PATH environment variable.
ifdef GCC_PATH
CC = $(GCC_PATH)/$(PREFIX)gcc
AS = $(GCC_PATH)/$(PREFIX)gcc -x assembler-with-cpp
CP = $(GCC_PATH)/$(PREFIX)objcopy
SZ = $(GCC_PATH)/$(PREFIX)size
else
CC = $(PREFIX)gcc
AS = $(PREFIX)gcc -x assembler-with-cpp
CP = $(PREFIX)objcopy
SZ = $(PREFIX)size
endif
HEX = $(CP) -O ihex
BIN = $(CP) -O binary -S
 
# CFLAGS
# cpu
CPU = -mcpu=cortex-m3

# fpu
# NONE for Cortex-M0/M0+/M3

# float-abi


# mcu
MCU = $(CPU) -mthumb $(FPU) $(FLOAT-ABI)

# macros for gcc
# AS defines
AS_DEFS = 

# C defines
C_DEFS =  \
-DUSE_FULL_LL_DRIVER \
-DHSE_VALUE=24000000 \
-DHSE_STARTUP_TIMEOUT=100 \
-DLSE_STARTUP_TIMEOUT=5000 \
-DLSE_VALUE=32768 \
-DMSI_VALUE=2097000 \
-DHSI_VALUE=16000000 \
-DLSI_VALUE=37000 \
-DVDD_VALUE=3300 \
-DPREFETCH_ENABLE=0 \
-DINSTRUCTION_CACHE_ENABLE=1 \
-DDATA_CACHE_ENABLE=1 \
-DSTM32L151xC \
-DUSE_FULL_LL_DRIVER \
-DHSE_VALUE=24000000 \
-DHSE_STARTUP_TIMEOUT=100 \
-DLSE_STARTUP_TIMEOUT=5000 \
-DLSE_VALUE=32768 \
-DMSI_VALUE=2097000 \
-DHSI_VALUE=16000000 \
-DLSI_VALUE=37000 \
-DVDD_VALUE=3300 \
-DPREFETCH_ENABLE=0 \
-DINSTRUCTION_CACHE_ENABLE=1 \
-DDATA_CACHE_ENABLE=1 \
-DSTM32L151xC


# AS includes
AS_INCLUDES = 

# C includes
C_INCLUDES =  \
-IInc \
-IInc/modules \
-IInc/crypto \
-IInc/system \
-IDrivers/STM32L1xx_HAL_Driver/Inc \
-IDrivers/STM32L1xx_HAL_Driver/Inc/Legacy \
-IDrivers/CMSIS/Device/ST/STM32L1xx/Include \
-IDrivers/CMSIS/Include \
-IDrivers/CMSIS/Include


# compile gcc flags
ASFLAGS = $(MCU) $(AS_DEFS) $(AS_INCLUDES) $(OPT) -Wall -fdata-sections -ffunction-sections

CFLAGS = $(MCU) $(C_DEFS) $(C_INCLUDES) $(OPT) -Wall -fdata-sections -ffunction-sections

ifeq ($(DEBUG), 1)
CFLAGS += -g -gdwarf-2
endif


# Generate dependency information
CFLAGS += -MMD -MP -MF"$(@:%.o=%.d)"


# LDFLAGS
# link script
LDSCRIPT = STM32L151CCUx_FLASH.ld

# libraries
LIBS = -lc -lm -lnosys 
LIBDIR = 
# LDFLAGS = $(MCU) -specs=nano.specs  -u _printf_float -T$(LDSCRIPT) $(LIBDIR) $(LIBS) -Wl,-Map=$(BUILD_DIR)/$(TARGET).map,--cref -Wl,--gc-sections
LDFLAGS = $(MCU) -specs=nano.specs  -u _printf_float -T$(LDSCRIPT) $(LIBDIR) $(LIBS) -Wl,-Map=$(BUILD_DIR)/$(TARGET).map,--cref -Wl,--gc-sections

# default action: build all
all: $(BUILD_DIR)/$(TARGET).elf $(BUILD_DIR)/$(TARGET).hex $(BUILD_DIR)/$(TARGET).bin
	@echo "building complited"
	@echo ""
	@echo "type \"sudo make flash\" to flash your device"
	@echo ""

# build the application
# list of objects
OBJECTS = $(addprefix $(BUILD_DIR)/,$(notdir $(C_SOURCES:.c=.o)))
vpath %.c $(sort $(dir $(C_SOURCES)))
# list of ASM program objects
OBJECTS += $(addprefix $(BUILD_DIR)/,$(notdir $(ASM_SOURCES:.s=.o)))
vpath %.s $(sort $(dir $(ASM_SOURCES)))

$(BUILD_DIR)/%.o: %.c Makefile | $(BUILD_DIR) 
	$(CC) -c $(CFLAGS) -Wa,-a,-ad,-alms=$(BUILD_DIR)/$(notdir $(<:.c=.lst)) $< -o $@

$(BUILD_DIR)/%.o: %.s Makefile | $(BUILD_DIR)
	$(AS) -c $(CFLAGS) $< -o $@

$(BUILD_DIR)/$(TARGET).elf: $(OBJECTS) Makefile
	$(CC) $(OBJECTS) $(LDFLAGS) -o $@
	$(SZ) $@

$(BUILD_DIR)/%.hex: $(BUILD_DIR)/%.elf | $(BUILD_DIR)
	$(HEX) $< $@
	
$(BUILD_DIR)/%.bin: $(BUILD_DIR)/%.elf | $(BUILD_DIR)
	$(BIN) $< $@	
	
$(BUILD_DIR):
	mkdir $@		

clean:
	-rm -fR $(BUILD_DIR)
	@echo ""

flash:
	stm32flash -R -E -w $(BUILD_DIR)/$(TARGET).hex /dev/ttyACM0
	@echo "flashing complited"
	@echo ""

# dependencies
-include $(wildcard $(BUILD_DIR)/*.d)

# *** EOF ***
